#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import os
import sys

class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        linea = self.rfile.read().decode("utf-8").split()
        aEjecutar = "mp32rtp -i 127.0.0.1 -p 23032 < " + AUDIO
        if linea[0] == "INVITE":
            self.wfile.write(b"SIP/2.0 100 Trying" + b"\r\n\r\n" +
            b"SIP/2.0 180 Ringing" + b"\r\n\r\n" +
            b"SIP/2.0 200 OK" + b"\r\n\r\n")
            os.system(aEjecutar)
            self.wfile.write(b"Audio acabado")
        elif linea[0] == "CANCEL" or linea[0] == "REGISTER" or linea[0] == "OPTIONS" or linea[0] == "PRACK" or linea[0] == "SUBSCRIBE" or linea[0] == "NOTIFY" or linea[0] == "PUBLISH" or linea[0] == "INFO" or linea[0] == "REFER" or linea[0] == "MESSAGE" or linea[0] == "UPDATE":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed" + b"\r\n\r\n")
        elif linea[0] == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
        elif linea[0] != "INVITE" or linea[0] != "ACK" or linea[0] != "BYE":
            self.wfile.write(b"SIP/2.0 400 Bad Request" + b"\r\n\r\n")

if __name__ == "__main__":
    try:
       IP = sys.argv[1]
       PORT = int(sys.argv[2])
       AUDIO = sys.argv[3]
    except ValueError:
       sys.exit("Usage: python3 server.py IP port audio_file")
    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    print("Listening...")
    serv.serve_forever()

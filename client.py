#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

lineaserver = sys.argv[2].split("@")
print(lineaserver)
lineapuerto = lineaserver[1].split(":")
print(lineapuerto)

METODO = sys.argv[1]
USER = lineaserver[0]
SERVER = lineapuerto[0]
PORT = int(lineapuerto[1])

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))
    my_socket.send(bytes(METODO, 'utf-8') + b" sip:" + bytes(USER, 'utf-8') + b"@" + bytes(SERVER, 'utf-8') + b" SIP/2.0" + b"\r\n\r\n")
    data = my_socket.recv(1024)
    if data.decode('utf-8') == ("SIP/2.0 100 Trying" + "\r\n\r\n" +
    "SIP/2.0 180 Ringing" + "\r\n\r\n" +
    "SIP/2.0 200 OK" + "\r\n\r\n"):
        my_socket.send(b"ACK sip:" + bytes(USER, 'utf-8') + b"@" + bytes(SERVER, 'utf-8') + b" SIP/2.0" + b"\r\n\r\n")

    print(data.decode('utf-8'))
    print("Terminando socket...")

print("Fin.")
